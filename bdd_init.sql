--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 13.2 (Debian 13.2-1.pgdg100+1)

--
-- Name: neighbor_ennemies; Type: TABLE; Schema: public;
--

CREATE TABLE public.neighbor_ennemies (
    variety_id_1 integer NOT NULL,
    variety_id_2 integer NOT NULL
);


--
-- Name: neighbor_friends; Type: TABLE; Schema: public;
--

CREATE TABLE public.neighbor_friends (
    variety_id_1 integer NOT NULL,
    variety_id_2 integer NOT NULL
);

--
-- Name: variety; Type: TABLE; Schema: public;
--

CREATE TABLE public.variety (
    id bigint NOT NULL,
    name character varying NOT NULL,
    column_space numeric NOT NULL,
    row_space numeric NOT NULL,
    occupation_month_number integer NOT NULL,
    best_exposition smallint NOT NULL,
    rotation character varying(255),
    plantation_months character varying,
    semis_months character varying,
    custom boolean NOT NULL
);

--
-- Name: variety_id_seq; Type: SEQUENCE; Schema: public;
--

CREATE SEQUENCE public.variety_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: variety_id_seq; Type: SEQUENCE OWNED BY; Schema: public;
--

ALTER SEQUENCE public.variety_id_seq OWNED BY public.variety.id;


--
-- Name: variety id; Type: DEFAULT; Schema: public;
--

ALTER TABLE ONLY public.variety ALTER COLUMN id SET DEFAULT nextval('public.variety_id_seq'::regclass);


--
-- Data for Name: neighbor_ennemies; Type: TABLE DATA; Schema: public;
--

COPY public.neighbor_ennemies (variety_id_1, variety_id_2) FROM stdin;
3	2
2	3
26	32
32	26
12	4
4	12
27	12
12	27
28	12
12	28
20	1
1	20
6	1
6	21
1	6
21	6
2	21
21	2
9	32
32	9
18	10
10	18
7	12
12	7
7	21
21	7
12	31
8	31
9	31
26	31
12	29
29	12
8	21
8	25
8	32
21	8
25	8
32	8
7	5
5	7
25	7
7	25
27	31
31	27
31	28
28	31
14	7
7	14
31	1
31	2
31	15
1	31
2	31
15	31
15	21
21	15
3	21
21	3
1	12
1	21
1	29
1	32
12	1
21	1
29	1
32	1
\.


--
-- Data for Name: neighbor_friends; Type: TABLE DATA; Schema: public;
--

COPY public.neighbor_friends (variety_id_1, variety_id_2) FROM stdin;
1	2
12	26
4	26
1	26
8	12
4	7
4	10
4	5
3	4
1	4
1	6827
6827	4
10	20
6	10
6827	6
3	6
2	32
2	29
6827	18
9	10
6827	7
6827	10
6827	32
6827	12
18	27
18	28
3	18
4	18
9	12
6827	31
1	6828
7	27
7	28
3	7
5	6828
6828	12
6828	26
6828	14
2	6829
3	6829
10	6829
12	6829
6828	6832
10	12
1	6832
4	6832
8	6832
10	27
10	28
1	6833
4	6833
2	10
7	10
3	10
6834	2
29	31
7	29
15	29
11	27
11	28
11	31
6834	26
6834	4
6834	6827
8	26
1	8
5	8
6834	6828
6834	8
6834	25
6834	28
6834	27
3	32
10	32
12	32
6834	14
6834	3
1	25
4	25
3	6835
1	6835
6827	6835
6835	26
6835	7
6835	10
1	28
4	28
12	28
1	27
4	27
12	27
6835	5
6835	18
6835	25
1	14
4	14
8	14
21	31
6835	28
15	31
6835	27
6835	14
3	6836
1	6836
6827	6836
6836	26
6836	7
6836	10
6836	5
6836	18
6836	25
3	12
6836	28
3	31
6836	27
1	3
6836	14
6838	2
6838	26
6838	4
6838	6827
6838	6828
6838	8
6838	25
6838	28
6838	27
6838	14
6838	3
18	6841
7	6841
10	6841
11	6841
1	6841
4	6841
12	6841
1	6843
6843	32
6843	29
6843	6829
6843	10
4	6844
6844	6828
6844	8
6827	6845
3	6845
6845	10
1	6849
6849	4
6849	6
6849	18
6849	7
6849	6829
6849	10
6849	32
6849	12
6849	31
3	6850
1	6850
6827	6850
6850	26
6850	7
6850	10
6850	5
6850	18
6850	25
6850	28
6850	27
6850	14
6853	29
6853	31
8	6854
6827	6854
9	6854
6828	6854
10	6854
3	6854
6854	26
6854	6829
6854	32
6854	28
6854	27
6856	27
6856	28
6856	31
4	6857
6827	6857
3	6857
6857	27
6857	28
6857	10
6857	29
\.


--
-- Data for Name: variety; Type: TABLE DATA; Schema: public;
--

COPY public.variety (id, name, column_space, row_space, occupation_month_number, best_exposition, rotation, plantation_months, semis_months, custom) FROM stdin;
25	piment	50	50	3	3	{}	{4}	{1,2,3}	f
26	ail	8	15	6	3	{}	{5,6,7}	{2,3,9,10}	f
27	poireau (hiver)	15	15	4	1	{}	{4,5}	{4,5}	f
28	poireau (été)	15	15	3	1	{}	{3,4,5,6}	{1,2,3,4}	f
21	pomme de terre	30	60	3	3	{}	{3,4,5}	{3,4,5}	f
22	topinambour	40	40	9	2	{}	{3,4}	{2,3}	f
23	patate douce	30	70	4	3	{}	{4,5,6}	{3}	f
24	poire de terre	50	100	7	3	{}	{2,3,4}	{}	f
29	mais	25	70	3	2	{}	{4,5,6}	{3,4,5}	f
30	asperge	30	60	36	3	{}	{2,3,4,5}	{1,2,3}	f
2	courgette	100	100	5	3	{}	{4,5,6}	{3,4}	f
1	tomate	90	90	2	3	{}	{3,4,5}	{2,3,4}	f
4	carotte	5	25	2	1	{}	{4,5,6}	{2,3}	f
3	radis	3	15	1	1	{}	{3,4,5,6,7,8,9,10}	{2,3,4,5,6,7,8}	f
6	concombre	30	120	2	3	{}	{4,5}	{2,3,4}	f
5	panais	15	35	4	2	{}	{3,4,5}	{3,4,5}	f
8	oignon	10	30	3	2	{}	{3,4,7,8}	{1,2}	f
7	épinard	4	25	2	1	{}	{4,5,6}	{2,3,4,5}	f
10	laitue	30	30	2	1	{}	{3,4,5,6,7}	{2,3,4}	f
9	échalotte	10	20	3	2	{}	{2,3,4,5,6}	{1,2}	f
12	betterave	10	15	2	1	{}	{3,4,5}	{3,4,5}	f
11	mache	5	25	3	1	{}	{3,4,7,8}	{2,3}	f
14	poivron	70	40	3	3	{}	{4,5,6}	{2,3,4}	f
13	lentille	15	50	3	3	{}	{2,3}	{1,2,3}	f
16	courge butternut	150	150	4	3	{}	{3,4,5}	{2,3,4}	f
15	potiron	250	250	7	2	{}	{3,4}	{3,4}	f
18	endive	25	30	2	1	{}	{6,7}	{4,5}	f
17	courge spaggethi	40	70	4	3	{}	{4,5}	{2,3}	f
20	chou fleur	55	60	3	3	{}	{2,3,4}	{1,2}	f
19	chou	45	90	3	3	{}	{4,5,6}	{2,3}	f
32	petit pois	10	50	2	1	{}	{4,5,6}	{2,3,4}	f
31	haricot vert	25	50	2	2	{}	{5,6}	{4,5}	f
6835	carotte jaune obtuse du doubs	5	25	2	1	\N	{}	{2,3,4,5,6}	t
6827	radis 18j	3	15	1	1	\N	{}	{1,2,3,4,5,6,7,8,9}	t
6828	oignon rouge - Pâle de niort	10	30	3	2	\N	{}	{2,3,4,8,9}	t
6836	carotte nantaise améliorée 3	5	25	2	1	\N	{}	{2,3,4,5,6}	t
6829	pois nain	3	40	2	1	\N	{}	{2,3,4,10,11}	t
6830	persil nain frisé mousse	8	25	2	2	\N	{}	{3,4,5,6,7,8}	t
6831	persil commun	8	25	4	2	\N	{}	{2,3,4,5,6,7,8,9}	t
6832	poivron petit marseillais	50	50	3	3	\N	{5,6}	{2,3,4}	t
6833	piment bio purple	50	50	3	3	\N	{5,6}	{2,3}	t
6834	tomate coeur de boeuf	90	90	2	3	\N	{4,5}	{2,3,4}	t
6837	alkekenge du pérou	55	55	6	1	\N	{5}	{2,3,4,5}	t
6838	tomate cerise	80	60	3	3	\N	{4,5}	{2,3,4,5}	t
6839	menthe poivrée	1	1	2	1	\N	{}	{3,4,5,6}	t
6841	poireau armor	15	15	3	1	\N	{3,4,5,6}	{3,4,5}	t
6842	estragon de russie	60	40	12	2	\N	{}	{3,4,5}	t
6843	courgette ambassador f1	100	100	5	3	\N	{5,6}	{3,4,5}	t
6844	panais white gem	15	35	4	2	\N	{}	{3,4,5,6,9}	t
6845	concombre marketer	30	120	2	3	\N	{5}	{3,4}	t
6846	ciboulette middleman	2	25	2	3	\N	{}	{3,4,5,6,7,8}	t
6847	oeillet d'inde double nain orange	20	20	6	3	\N	{}	{3,4,5}	t
6848	épinard fraise	25	25	2	1	\N	{}	{3,4,5,6,7,8}	t
6849	radis ostergrub rosa 2	3	15	1	2	\N	{}	{4,5}	t
6850	carotte marché de paris (mini)	2	10	4	1	\N	{}	{4,5,6,7}	t
6851	aneth bio commune	10	10	3	2	\N	{}	{4,5}	t
6852	engrais vers	1	1	2	2	\N	{}	{4,5,6,7,8}	t
6853	potimarron orange summer f1	100	100	7	2	\N	{5}	{4,5}	t
6854	betterave crapaudine	20	20	2	1	\N	{3,4,5}	{4,5,6}	t
6855	thym d'été annuel	2	25	1	2	\N	{}	{4,5,6,7,8}	t
6856	mache bio vit	5	25	3	1	\N	{}	{7,8,9,10}	t
6857	épinard monstrueux de viroflay	12	25	2	1	\N	{}	{8,9,10}	t
6858	ciboulette commune	2	20	2	3	\N	{}	{2,3,4,5}	t
6859	camomille commune	2	40	4	2	\N	{}	{3,4}	t
6840	basilic à feuille de laitue	30	30	6	2	\N	{4,5}	{3,4,5}	t
\.


--
-- Name: variety_id_seq; Type: SEQUENCE SET; Schema: public;
--

SELECT pg_catalog.setval('public.variety_id_seq', 32, true);


--
-- Name: neighbor_ennemies neighbor_ennemies_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.neighbor_ennemies
    ADD CONSTRAINT neighbor_ennemies_pkey PRIMARY KEY (variety_id_1, variety_id_2);


--
-- Name: neighbor_friends neighbor_friends_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.neighbor_friends
    ADD CONSTRAINT neighbor_friends_pkey PRIMARY KEY (variety_id_1, variety_id_2);


--
-- Name: variety variety_pkey; Type: CONSTRAINT; Schema: public;
--

ALTER TABLE ONLY public.variety
    ADD CONSTRAINT variety_pkey PRIMARY KEY (id);


--
-- Name: fk2; Type: INDEX; Schema: public;
--

CREATE INDEX fk2 ON public.neighbor_friends USING btree (variety_id_2);


--
-- Name: fki_vareity_id_1_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_vareity_id_1_fk ON public.neighbor_ennemies USING btree (variety_id_1);


--
-- Name: fki_variety_id_2_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_variety_id_2_fk ON public.neighbor_ennemies USING btree (variety_id_2);


--
-- Name: fki_variety_id_2_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_variety_id_2_fkey ON public.neighbor_friends USING btree (variety_id_1);


--
-- Name: neighbor_ennemies fk2m8e956led5pvvp7vfsv8lf06; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.neighbor_ennemies
    ADD CONSTRAINT fk2m8e956led5pvvp7vfsv8lf06 FOREIGN KEY (variety_id_1) REFERENCES public.variety(id);


--
-- Name: neighbor_ennemies fk6yk117a0mpke42m7fp8s2xrr1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.neighbor_ennemies
    ADD CONSTRAINT fk6yk117a0mpke42m7fp8s2xrr1 FOREIGN KEY (variety_id_2) REFERENCES public.variety(id);


--
-- Name: neighbor_friends fk7sxq6cvn3nm6wihn56km037m7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.neighbor_friends
    ADD CONSTRAINT fk7sxq6cvn3nm6wihn56km037m7 FOREIGN KEY (variety_id_1) REFERENCES public.variety(id);


--
-- Name: neighbor_friends fkif9ike5g0fi82si6nsejl9lui; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.neighbor_friends
    ADD CONSTRAINT fkif9ike5g0fi82si6nsejl9lui FOREIGN KEY (variety_id_2) REFERENCES public.variety(id);


--
-- Name: neighbor_ennemies variety_id_1_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.neighbor_ennemies
    ADD CONSTRAINT variety_id_1_fk FOREIGN KEY (variety_id_1) REFERENCES public.variety(id);


--
-- Name: neighbor_friends variety_id_1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.neighbor_friends
    ADD CONSTRAINT variety_id_1_fkey FOREIGN KEY (variety_id_1) REFERENCES public.variety(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: neighbor_ennemies variety_id_2_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.neighbor_ennemies
    ADD CONSTRAINT variety_id_2_fk FOREIGN KEY (variety_id_2) REFERENCES public.variety(id);


--
-- Name: neighbor_friends variety_id_2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.neighbor_friends
    ADD CONSTRAINT variety_id_2_fkey FOREIGN KEY (variety_id_2) REFERENCES public.variety(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

