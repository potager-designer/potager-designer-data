PGDMP         #                v           potagerdesigner    9.6.6    10.1 	    e           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            f           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �            1259    16404    variety    TABLE     }  CREATE TABLE variety (
    id bigint NOT NULL,
    name character varying NOT NULL,
    column_space numeric NOT NULL,
    row_space numeric NOT NULL,
    occupation_month_number integer NOT NULL,
    best_exposition smallint NOT NULL,
    rotation character varying(255),
    plantation_months character varying,
    semis_months character varying,
    custom boolean NOT NULL
);
    DROP TABLE public.variety;
       public         postgres    false            �            1259    16402    variety_id_seq    SEQUENCE     p   CREATE SEQUENCE variety_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.variety_id_seq;
       public       postgres    false    186            g           0    0    variety_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE variety_id_seq OWNED BY variety.id;
            public       postgres    false    185            �           2604    16407 
   variety id    DEFAULT     Z   ALTER TABLE ONLY variety ALTER COLUMN id SET DEFAULT nextval('variety_id_seq'::regclass);
 9   ALTER TABLE public.variety ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    186    185    186            b          0    16404    variety 
   TABLE DATA               �   COPY variety (id, name, column_space, row_space, occupation_month_number, best_exposition, rotation, plantation_months, semis_months, custom) FROM stdin;
    public       postgres    false    186   �	       h           0    0    variety_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('variety_id_seq', 32, true);
            public       postgres    false    185            �           2606    16412    variety variety_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY variety
    ADD CONSTRAINT variety_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.variety DROP CONSTRAINT variety_pkey;
       public         postgres    false    186            b   �  x��V]��6~֜Bo����$�X� ��$J�[2dy��`�R�-���JҒe����ȏ�GRE�z�)�Y�㧄�콂�Ȋ��`���aR�l�D͚`PgM���`�2��ݎ�V;%G���ߔ���TLL^�:��}�~�����`]f`�5�IVJ ��?+�s��9kq�� (���6�;�ѱ*�ρ�zʁ,K�K/=�����:V$p�L��{b�9 �u��N��I=��Fe=��D!�z�sJ��e�Z�N��Uy�(.~�-өb�� +v��Sl
5W�b'�,:��j�zF�B��9y�dJjֺS�.�1D��"���d���2Q$*" Ii�`�O�{f��Xɣ�raG1�Zh�c�;
�����̢K��J�G�X��˒���n��SX�8��'�`Gl��od-��efB�bN�Љ�<L��^Rᛃ�H��F3UQ��\�����A'WIW�	���ɚ� �o��r�co�F��w��h�gʜa1PV��S]c��`��~Ә�zC���͎��*��^�8��2eU�s�V����kϡ���Ο���I�O�s�v�"�"�ȔV�)��?�h�G?�BF�7�!�ݏ�[y��v���b�3�_�2Ma���ΎP����ZZ[F[`�Fgtv ;(�6�x�J�e����~�/���5: Z�S���y&D�C�ݠ[����ǝwv��"�TW��"�a�t���6�M������t��Q���"�X���e8���-�G׷�3L@TaGP,v�\�Q�	#܎��U�*s%��;���%�?یP�9��ZB�c��2#�XA>7ũ(�c�٢i��Vb>��u�-N8��-�QL��+T�w�\��V�ga�� )ӽ��0ȳu�"V�/�{���=��4���[\����&`E��
��`.�V��ܟ
E��%��>�[�I�z�g�3�V;fQm���m���8�j� �$4O|����`?�7�P
Y?��"�!,;@fW7aC�q�̕�〦�*���n0�@���7�6�F�鱗���$�`��:�Q�t:����J����:�1����p����a���J$��^E��e�''{9���E/�RNG��5�_�ޯ\3�vj���j3w�Û�����"��|�Eh`pF5�����{i�/�U�W��R���[y�j8����r����jb��r�[}⏿�E�~��T���������˿�4Bd     